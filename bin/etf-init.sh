#!/bin/bash
set -e

_term() {
  if [[ -f /var/run/crond.pid ]]; then
    kill -9 $(cat /var/run/crond.pid)
    rm -f /var/run/crond.pid
  fi
  rm -rf /opt/omd/sites/etf/etc/nagios/conf.d/wlcg/
  if [[ -f /omd/sites/etf/tmp/run/live-tls ]]; then
    rm -f /omd/sites/etf/tmp/run/live-tls
  fi
  if [[ -f /omd/sites/etf/.oidc-agent/oidc-env.sh ]]; then
    source /omd/sites/etf/.oidc-agent/oidc-env.sh
    oidc-agent --kill
  fi
  omd stop
  /usr/sbin/httpd -k stop
}

trap _term SIGINT SIGTERM

function print_header() {
  ncgx_version=$(rpm -q --qf "%{VERSION}-%{RELEASE}" ncgx)
  echo "ETF version: ${ncgx_version} Copyright CERN 2016"
  echo "License: https://gitlab.cern.ch/etf/ncgx/blob/master/LICENSE"
  echo "Check_MK version: $CHECK_MK_VERSION"
  echo "Copyright by Mathias Kettner (https://mathias-kettner.de/check_mk.html)"
  plugins=$(rpm -qa | grep nagios-plugins)
  echo "Plugins:"
  echo "${plugins}"
  echo ""
}

function etf_update() {
  yum -y update python-nap ncgx python-vofeed-api nagios-stream
}

function start_xinetd() {
  echo "Starting xinetd ..."
  export XINETD_LANG="en_US" && /usr/sbin/xinetd -stayalive -pidfile /var/run/xinetd.pid
}

function create_system_apache_config() {
    # We have the special situation that the site apache is directly accessed from
    # external without a system apache reverse proxy. We need to disable the canonical
    # name redirect here to make redirects work as expected.
    #
    # In a reverse proxy setup the proxy would rewrite the host to the host requested by the user.
    # See omd/packages/apache-omd/skel/etc/apache/apache.conf for further information.
    #
    # The ServerName also needs to be in a special way. See
    # (https://forum.checkmk.com/t/check-mk-running-behind-lb-on-port-80-redirects-to-url-including-port-5000/16545)
    APACHE_DOCKER_CFG="/omd/sites/$CHECK_MK_SITE/etc/apache/conf.d/cmk_docker.conf"
    echo -e "# Created for Checkmk docker container\n\nUseCanonicalName Off\nServerName 127.0.0.1\n" >"$APACHE_DOCKER_CFG"
    chown "$CHECK_MK_SITE:$CHECK_MK_SITE" "$APACHE_DOCKER_CFG"
    # Redirect top level requests to the sites base url
    echo -e "# Redirect top level requests to the sites base url\nRedirectMatch 302 ^/$ /$CHECK_MK_SITE/\n" >>"$APACHE_DOCKER_CFG"
}

function etf_init() {
  if [ -z "$CHECK_MK_SITE" ]; then
    echo "ERROR: No site ID given"
    exit 1
  fi

  if [[ -n ${CHECK_MK_USER_ID} ]] ; then
   echo "Changing ${CHECK_MK_SITE} uid to ${CHECK_MK_USER_ID}"
   /usr/sbin/usermod -u "${CHECK_MK_USER_ID}" "${CHECK_MK_SITE}"
   chown -R "${CHECK_MK_SITE}" /etc/ncgx /var/cache/ncgx /var/cache/nap
   chown -R "${CHECK_MK_SITE}" /usr/libexec/grid-monitoring/probes/
  fi
  if [[ -n ${CHECK_MK_GROUP_ID} ]] ; then
     echo "Creating group with gid ${CHECK_MK_GROUP_ID}"
     /usr/sbin/groupadd -g "${CHECK_MK_GROUP_ID}" sec
     /usr/sbin/groupmems -g sec -a "${CHECK_MK_SITE}"
  fi

  echo "Initialising ..."
  if [[ -d /opt/omd/sites/etf/etc/nagios/conf.d/wlcg/ ]]; then
      rm -rf /opt/omd/sites/etf/etc/nagios/conf.d/wlcg/
  fi

  /usr/bin/omd stop
}

function copy_certs() {
  echo "Copying certificates ..."
  if [[ ! -f /etc/grid-security/hostcert.pem ]]; then
      echo "Failed to find certificates in /etc/grid-security"
      exit
  fi
  mkdir -p /opt/omd/sites/etf/etc/nagios/globus/
  cp /etc/grid-security/host*.pem /opt/omd/sites/etf/etc/nagios/globus/
  cp /etc/grid-security/etf_srv*.pem /opt/omd/sites/etf/etc/nagios/globus/
  chown -R "${CHECK_MK_SITE}"."${CHECK_MK_SITE}" /opt/omd/sites/etf/etc/nagios/globus/
}

function config_mail_relay() {
  echo "Configuring mail relay ..."
  # Prepare local MTA for sending to smart host
  # TODO: Syslog is only added to support postfix. Can we please find a better solution?
  if [ -n "$MAIL_RELAY_HOST" ]; then
      echo "Configuring mail relay (Hostname: $HOSTNAME, Relay host: $MAIL_RELAY_HOST)"
      echo "$HOSTNAME" >/etc/mailname
      postconf -e myorigin="$HOSTNAME"
      postconf -e mydestination="$(hostname -a), $(hostname -A), localhost.localdomain, localhost"
      postconf -e relayhost="$MAIL_RELAY_HOST"
      postconf -e mynetworks="127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128"
      postconf -e mailbox_size_limit=0
      postconf -e recipient_delimiter=+
      postconf -e inet_interfaces=all
      postconf -e inet_protocols=all

      echo "Starting mail services ..."
      syslogd
      /etc/init.d/postfix start
  fi
}

function apache_init() {
  if [ "$APACHE_PROXY_ONLY" = "on" ]; then
    omd config "$CHECK_MK_SITE" set APACHE_TCP_ADDR 0.0.0.0
    omd config "$CHECK_MK_SITE" set APACHE_TCP_PORT 5000

    create_system_apache_config
  else
    omd config "$CHECK_MK_SITE" set APACHE_MODE own
    sed -i "s/ETF_ADMIN_REGEX/${ETF_ADMINS}/" /etc/httpd/conf.d/ssl.conf
  fi
}

function set_cmk_passwd() {
  if [[ -z "${CMK_PASSWORD}" ]]; then
    echo "Keeping existing cmkadmin password"
  else
    echo "Setting cmkadmin password"
    su etf -c "htpasswd -b -m ~/etc/htpasswd cmkadmin ${CMK_PASSWORD}"
  fi
}

function config_web_access() {
  if [ -n "$CHECK_MK_ADMINS" ]; then
    echo "Configuring access ..."
    echo "Configured admins: $CHECK_MK_ADMINS"
    sed -i "s|admin_users.*|admin_users = [$CHECK_MK_ADMINS]|" /opt/omd/sites/${CHECK_MK_SITE}/etc/check_mk/multisite.mk
  fi
}

function config_alerts() {
  if [[ -f /etc/check_mk/contacts.mk ]]; then
    cp -f /etc/check_mk/contacts.mk /opt/omd/sites/${CHECK_MK_SITE}/etc/check_mk/conf.d/wato/
  fi
  if [[ -f /etc/check_mk/users.mk ]]; then
      cp -f /etc/check_mk/users.mk /opt/omd/sites/${CHECK_MK_SITE}/etc/check_mk/conf.d/wato/
  fi
  if [[ "${ETF_ALERTS_ENABLED}" -eq "1" ]] && [[ -f /etc/check_mk/notifications.mk ]]; then
      echo "Enabling notifications ..."
      cp -f /etc/check_mk/notifications.mk /opt/omd/sites/${CHECK_MK_SITE}/etc/check_mk/conf.d/wato/
      sed -i "s/ETF_HOSTED_BY/${ETF_HOSTED_BY}/" /opt/omd/sites/${CHECK_MK_SITE}/etc/check_mk/conf.d/wato/notifications.mk
      sed -i "s/ETF_NAGIOS_HOST/${ETF_NAGIOS_HOST}/" /opt/omd/sites/${CHECK_MK_SITE}/etc/check_mk/conf.d/wato/notifications.mk
  fi
}

function init_api() {
  touch /var/www/html/status_snapshot.json && chown etf.etf /var/www/html/status_snapshot.json
  mkdir -p /var/www/html/etf-raw && chown etf.etf /var/www/html/etf-raw
}

function config_etf() {
  echo "Configuring main.mk: $ETF_HOSTED_BY"
  if [[ -z "${ETF_HOSTED_BY}" ]]; then
     echo "   Variable ETF_HOSTED_BY is not defined, not touching main.mk"
  else
     grep -qF "${ETF_HOSTED_BY}" /opt/omd/sites/"${CHECK_MK_SITE}"/etc/check_mk/main.mk || echo "all_hosts += [ \"${ETF_HOSTED_BY}\" ]" >> /opt/omd/sites/"${CHECK_MK_SITE}"/etc/check_mk/main.mk
  fi

  echo "Configuring ETF ..."
  if [[ -z "${ETF_NAGIOS_HOST}" ]]; then
      echo "   Variable ETF_NAGIOS_HOST is not defined, using hostname"
      ETF_NAGIOS_HOST=$(hostname)
  fi

  grep -qF "${ETF_NAGIOS_HOST}" /etc/ncgx/ncgx.cfg || echo "NAGIOS_HOST = \"${ETF_NAGIOS_HOST}\"" >> /etc/ncgx/ncgx.cfg
  sed -i "s/ETF_NAGIOS_HOST/${ETF_NAGIOS_HOST}/" /etc/httpd/conf.d/welcome.conf
  sed -i "s/ETF_NAGIOS_HOST/${ETF_NAGIOS_HOST}/" /var/www/html/index.html
  sed -i "s/ETF_NAGIOS_HOSTXX/${ETF_NAGIOS_HOST}/" /etc/profile.d/grid-env.sh
}

function etf_start() {
  cp /etc/ncgx/templates/generic/handlers.cfg /opt/omd/sites/etf/etc/nagios/conf.d/

  omd start
  rm -f /opt/omd/sites/etf/etc/nagios/conf.d/handlers.cfg
  if [[ "${ETF_IPV6_ONLY}" -eq "1" ]] ; then
    su etf -c "ncgx --log --ipv6 | tee /opt/omd/sites/etf/var/log/ncgx.log"
  else
    su etf -c "ncgx --log | tee /opt/omd/sites/etf/var/log/ncgx.log"
  fi
  su - etf -c "cmk -II; cmk -O"
  if [[ "${NSTREAM_ENABLED}" -eq "1" ]] ; then
      echo "Nagios stream enabled ..."
  else
      echo "Nagios stream disabled ..."
      /usr/bin/disable_nstream
  fi
  echo "Reloading crontab ..."
  su etf -c "omd reload crontab"
}

function fix_live_ip6() {
  sed -i '/only_from/d' /omd/sites/etf/etc/xinetd.d/mk-livestatus
  echo "Reloading xinetd ..."
  su etf -c "omd reload xinetd"
}

function fix_cmk_theme() {
  echo "Fixing Checkmk theme ..."
  sed -i "s/facelift/classic/" /omd/sites/"${CHECK_MK_SITE}"/etc/check_mk/multisite.d/wato/global.mk
}

function etf_wait() {
  if [ "$APACHE_PROXY_ONLY" = "on" ]; then
    echo "Starting crond ..."
    sed -i -n '/pam_loginuid/!p' /etc/pam.d/crond
    /usr/sbin/crond -m off -p -n
  else
    echo "Starting crond ..."
    /usr/sbin/crond -m off -p -s
    echo "Starting Apache ..."
    /usr/sbin/httpd -DFOREGROUND
  fi
}

function config_htcondor() {
  echo "Configuring HT-Condor ..."
  if [[ -f /usr/bin/condor_status ]]; then
      /usr/bin/condor_status -schedd -af MyAddress -pool $(hostname) > /var/lib/condor/spool/.schedd_address
  fi
  if [[ ! -s /var/lib/condor/spool/.schedd_address ]]; then
      echo "Schedd address file empty, waiting 5 seconds before restarting ..."
      sleep 5
      exit 1
  fi
}

function generate_test_cert() {
  echo "Generating ETF test cert ..."
  if [[ ! -f /opt/omd/sites/etf/etc/nagios/globus/hostcert.pem ]]; then
      echo "Failed to find host certificate in /opt/omd/sites/etf/etc/nagios/globus/"
      exit
  fi
  cat /opt/omd/sites/etf/etc/nagios/globus/hostcert.pem > /opt/omd/sites/etf/etc/nagios/globus/testcert.pem
  cat /opt/omd/sites/etf/etc/nagios/globus/hostkey.pem >> /opt/omd/sites/etf/etc/nagios/globus/testcert.pem
  mv -f /opt/omd/sites/etf/etc/nagios/globus/testcert.pem /opt/omd/sites/etf/etc/nagios/globus/etf_testcert.pem
  chown -R "${CHECK_MK_SITE}"."${CHECK_MK_SITE}" /opt/omd/sites/etf/etc/nagios/globus/
}

function start_oidc_agent() {
  echo "Starting OIDC-agent ..."
  su etf -c "mkdir -p /omd/sites/etf/.oidc-agent"
  su etf -c "oidc-agent > /opt/omd/sites/etf/.oidc-agent/oidc-env.sh"
  cp -f /etc/grid-security/tokens/* /opt/omd/sites/etf/.oidc-agent/
  chown -R etf.etf /opt/omd/sites/etf/.oidc-agent/
}

function etf_container_init() {
  print_header
  etf_update
  start_xinetd
  etf_init
  copy_certs
  apache_init
  config_web_access
  config_alerts
  init_api
  config_htcondor
  config_etf
  fix_cmk_theme
  etf_start
}
