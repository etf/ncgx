#!/usr/bin/env bash

now=`date +%s`

cmdpipe=
if [ -p /var/nagios/rw/nagios.cmd ]; then
   cmdpipe='/var/nagios/rw/nagios.cmd'
fi
if [ -p /omd/sites/etf/tmp/run/nagios.cmd ]; then
   cmdpipe='/omd/sites/etf/tmp/run/nagios.cmd'
fi

if [ ! -p $cmdpipe ]; then
   printf "Unable to find Nagios command pipe"
   exit 1
fi

/usr/bin/printf "[%lu] START_EXECUTING_SVC_CHECKS\n" $now > $cmdpipe
