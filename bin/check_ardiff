#!/usr/bin/env python
#
# A/R diff script for SAM3
# Authors: Salvatore Tupputi, Marian Babik
# Copyright (C) CERN 2016
# License: http://www.apache.org/licenses/LICENSE-2.0
#
#


import datetime
import json
import urllib.request
import urllib.error
import urllib.parse
import argparse
import io
import sys


def diff(args):
    start = (datetime.datetime.now() - datetime.timedelta(days=args.days)).strftime('%Y-%m-%d')
    end = (datetime.datetime.now() - datetime.timedelta(days=args.delay)).strftime('%Y-%m-%d')

    prod_link = args.prod_link
    pre_link = args.pre_link

    prod_prof = args.prod_prof
    pre_prof = args.pre_prof

    prod_url = 'http://' + prod_link + \
               '/dashboard/request.py/getstatsresultsmin?prettyprint&plot_type=quality&start_time=' + \
               start + 'T00:00:00Z&group=All%20sites&profile_name=' + prod_prof + '&end_time=' + \
               end + 'T00:00:00Z&granularity=daily&view=siteavl'
    pre_url = 'http://' + pre_link + \
              '/dashboard/request.py/getstatsresultsmin?prettyprint&plot_type=quality&start_time=' + \
              start + 'T00:00:00Z&group=All%20sites&profile_name=' + pre_prof + '&end_time=' + \
              end + 'T00:00:00Z&granularity=daily&view=siteavl'

    prod_json = json.load(urllib.request.urlopen(prod_url))
    pre_json = json.load(urllib.request.urlopen(pre_url))

    pro_data = prod_json['data']
    pre_data = pre_json['data']

    report = io.StringIO()

    report.write('prod::' + prod_prof + ' vs qa::' + pre_prof + ' from ' + start + ' to ' + end)
    report.write('{0:30s} {1:6s} {2:6s} {3:6s} {4:6s} {5:6s} {6:2s}'.format('\nSITE', 'PROD', 'PREP', 'W_PROD', 'W_PREP', 'DIFF', '%'))

    cache = []

    for pro in pro_data:
        res = [pre for pre in pre_data if pro['name'] == pre['name']]

        try:
            comp = float(res[0]['data'][0]['OK'])
            ok_comp = float(res[0]['data'][0]['OK'])
            cr_comp = float(res[0]['data'][0]['CRIT'])
            dw_comp = float(res[0]['data'][0]['SCHED'])
            if sum([ok_comp, cr_comp, dw_comp]) == 0:
                w_comp = 0
            else:
                w_comp = ok_comp / sum([ok_comp, cr_comp, dw_comp])
        except IndexError as e:
            report.write('\nMISSING ' + pro['name'].split(' ')[0] + ' FROM PREPROD')
        else:
            ref = float(pro['data'][0]['OK'])
            ok_ref = float(pro['data'][0]['OK'])
            cr_ref = float(pro['data'][0]['CRIT'])
            dw_ref = float(pro['data'][0]['SCHED'])
            if sum([ok_ref, cr_ref, dw_ref]) == 0:
                w_ref = 0
            else:
                w_ref = ok_ref / sum([ok_ref, cr_ref, dw_ref])
            delta = w_comp - w_ref
            if abs(delta) > args.cut:
                cache.append((pro['name'], str(ref), str(comp), w_ref, w_comp, delta, 100 * delta))

    for entry in sorted(cache, key=lambda x: x[6]):
        report.write(
            '{0:30s} {1:6s} {2:6s} {3:6.2f} {4:6.2f} {5:6.2f} {6:<4.2f}'.format('\n' + entry[0], entry[1], entry[2], entry[3], entry[4], entry[5], entry[6]))

    report.seek(0)
    fl = len([l for l in report.readlines()])-2

    if fl > args.threshold:
        print("CRITICAL: %d site(s) with differences over threshold | diffs=%d" % (fl, fl))
        print(report.getvalue())
        sys.exit(2)
    else:
        print("OK: diff is fine | diffs=%d" % fl)
        print(report.getvalue())
        sys.exit(0)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='ardiff - Computes difference between two SAM3 profiles')
    parser.add_argument('--version', action='version', version='%(prog)s 1.0')
    parser.add_argument('-c', '--cut', default=0.01, type=float,
                        help='Cutoff rate for A/R')
    parser.add_argument('-d', '--days', default=1, type=int,
                        help='Time period in days that determines the start time (defaults to 1, so yesterday)')
    parser.add_argument('-D', '--delay', default=0, type=int,
                        help='Time delay for the end time (defaults to 0, so now), time_period = (now-delay) to (now-days)')
    parser.add_argument('--prod-link', dest='prod_link',
                        help='Hostname of SAM3 instance (will be referred to as production')
    parser.add_argument('--qa-link', dest='pre_link',
                        help='Hostname of SAM3 instance (will be referred to as QA')
    parser.add_argument('--prod-profile', dest='prod_prof',
                        help='A/R profile to query on production instance')
    parser.add_argument('--qa-profile', dest='pre_prof',
                        help='A/R profile to query on QA instance')
    parser.add_argument('-t', '--threshold', default=10, type=int,
                        help='Number of differences considered critical')

    diff(parser.parse_args())

