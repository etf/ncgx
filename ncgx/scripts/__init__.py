import logging
import sys
import traceback
import pkgutil
import os.path
from future.utils import string_types

from ncgx.config import base_config
import ncgx.scripts

log = logging.getLogger('ncgx')


def load_scripts(inventory, hosts):
    if 'SCRIPTS' not in base_config.keys():
        return
    log.info("Configured post-processing scripts: %s" % base_config['SCRIPTS'])
    assert not isinstance(base_config['SCRIPTS'], string_types)

    for script in base_config['SCRIPTS']:
        try:
            default_pkg = os.path.dirname(ncgx.scripts.__file__)
            # change to importlib.import_module for 2.7+
            if script in [name for _, name, _ in pkgutil.iter_modules([default_pkg])]:
                mod = __import__("ncgx.scripts.%s" % script, None, None, base_config['SCRIPTS'])
            else:
                log.info("Configured script not found")
                continue
            #    mod = __import__("x_plugins.%s" % adaptor, None, None, base_config['PLUGINS'].keys())
        except ImportError as e:
            log.error("Exception %s while loading script %s" % (e, script))
            sys.exit(-1)

        try:
            log.info("Calling post-processing script: %s" % script)
            mod.run(inventory, hosts=hosts)
        except Exception as e:
            log.error("Exception was thrown while running script %s" % script)
            log.error("%s" % e)
            log.error("Stack trace:")
            traceback.print_exc(file=sys.stderr)
            sys.exit(-1)

