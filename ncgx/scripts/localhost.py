import logging
import ncgx.config

log = logging.getLogger('ncgx')


# Change localhost to NAGIOS_HOST
def run(inventory, hosts=None):
    if 'localhost' in inventory.keys() and 'NAGIOS_HOST' in ncgx.config.base_config.keys():
        inventory[ncgx.config.base_config['NAGIOS_HOST']] = inventory.pop('localhost')


