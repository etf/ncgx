import logging

log = logging.getLogger('ncgx')


# Compute unique tag across given host:
# 1. try to determine unique tag from host tags
# This is used to compute service types
def run(inventory, hosts=None):
    if not hosts:
        log.warning("Unable to compute unique tags, missing hosts")
        return

    for host in inventory.keys():
        host_tags = hosts.get_tags(host=host)
        for metric in inventory[host].keys():
            metric_def = inventory[host][metric]
            if '_unique_tag' in metric_def.keys():  # skip if it's there already
                continue
            if '_tags' in metric_def.keys():
                tag_list = set(metric_def['_tags'].split())
                if len(tag_list) == 1:
                    metric_def["_unique_tag"] = tag_list.pop()
                else:
                    # multiple tags, try to resolve via host groups
                    if host_tags and len(tag_list & host_tags) == 1:
                        metric_def["_unique_tag"] = (tag_list & host_tags).pop()
                    # log failure
                    else:
                        log.warning("Failed to compute unique tag for %s/%s" % (host, metric))
                        metric_def["_unique_tag"] = " ".join(host_tags)
