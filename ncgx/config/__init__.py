import os
import sys
import logging
import glob

from ncgx import settings

__all__ = ['base_config', 'metrics', 'metrics_d']

log = logging.getLogger('ncgx')

base_config = {}
metrics = {}
metrics_d = list()


def load_base_config():
    if not os.path.exists(settings.CONFIG_PATH):
        log.error("Default config path access error (%s)" % settings.CONFIG_PATH)
        sys.exit(1)

    _mcf = os.path.join(settings.CONFIG_PATH, "metrics.%s" % settings.CONFIG_EXT)

    metric_files = [f for f in glob.glob(settings.METRICS_PATH + "/*.%s" % settings.CONFIG_EXT)
                        if os.path.isfile(os.path.join(settings.METRICS_PATH, f))]

    if not os.path.exists(_mcf) and not metric_files:
        log.error("Metrics path access error (%s/%s)" % (_mcf, settings.METRICS_PATH))
        sys.exit(1)

    global metrics
    global metrics_d
    if os.path.exists(_mcf):
        settings.ncxg_exec(_mcf, {}, metrics)
    else:
        for mf in sorted(metric_files):
            m_config = dict()
            settings.ncxg_exec(os.path.join(settings.METRICS_PATH, mf), {}, m_config)
            metrics_d.append(m_config)

    log.debug("metrics: %s" % metrics)
    log.debug("metrics_d: %s" % metrics_d)
    if not metrics and not metrics_d:
        log.error("No metric templates found, check /etc/ncgx/metrics.cfg or /etc/ncgx/metrics.d")
        sys.exit(1)

    _bcf = os.path.join(settings.CONFIG_PATH, "ncgx.%s" % settings.CONFIG_EXT)

    if not os.path.exists(_bcf):
        log.error("Unable to read configuration from %s" % _bcf)
        sys.exit(1)

    global base_config
    assert isinstance(base_config, dict)

    settings.ncxg_exec(_bcf, {}, base_config)
    log.debug("base config: %s" % base_config)


load_base_config()
