import os
import sys
import glob
import logging

from ncgx import settings

__all__ = ['local_config']

local_config = list()

log = logging.getLogger('ncgx')


def load_local_config():
    if not os.path.exists(settings.CONFIG_PATH):
        log.error("Default config path access error (%s)" % settings.CONFIG_PATH)
        sys.exit(1)

    cf_files = [f for f in glob.glob(settings.LOCAL_CONFIG_PATH + "/*.%s" % settings.CONFIG_EXT)
                if os.path.isfile(os.path.join(settings.LOCAL_CONFIG_PATH, f))]

    global local_config

    for cf in cf_files:
        c_config = dict()
        settings.ncxg_exec(os.path.join(settings.LOCAL_CONFIG_PATH, cf), {}, c_config)
        log.debug("custom config: %s" % c_config)

        local_config.append(c_config)


load_local_config()

