import logging

log = logging.getLogger('ncgx')


def template_function(line, base_config, context):
    if 'DEFINE_HOSTGROUPS_MEMBERS' in base_config.keys() and base_config['DEFINE_HOSTGROUPS_MEMBERS']:
        return "members        "+",".join(context['members'])
    else:
        return " "
