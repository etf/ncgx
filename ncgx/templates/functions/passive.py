import logging

log = logging.getLogger('ncgx')


def template_function(line, base_config, context):
    log.debug('  tfunction(passive)')
    if 'passive' in context.keys() and context['passive']:
        return "active_checks_enabled           0\n    passive_checks_enabled          1\n"
    else:
        return " "

