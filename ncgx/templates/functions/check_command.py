import logging

log = logging.getLogger('ncgx')


def template_function(line, base_config, context):
    log.debug('  t_function(check_command)')
    metric_def = context
    if 'COMMAND_OVERRIDE' in base_config.keys():
        for k, v in base_config['COMMAND_OVERRIDE'].items():
            if k == 'ALL' or k in metric_def['metric']:
                log.info("          Applying command override: %s (%s)" % (v, k))
                return v

    if 'passive' in metric_def.keys() and metric_def['passive']:
        return 'ncgx_check_passive!This metric is passive and cannot be executed independently'

    if 'args' in metric_def.keys():
        a_tuples = [(k, v) for k, v in metric_def['args'].items()]
        args = " ".join("%s %s" % tup for tup in a_tuples)
        log.debug(metric_def)
        if 'nagios_command' in metric_def:
            c_line = "%s!%s!%s" % (metric_def['nagios_command'], metric_def['command'], args)
        elif 'target' in metric_def.keys() or metric_def['hostname'] == 'localhost' or (
                        'NAGIOS_HOST' in base_config.keys() and
                        metric_def['hostname'] == base_config['NAGIOS_HOST']):
            c_line = "ncgx_check!%s!%s" % (metric_def['command'], args)
        else:
            c_line = "ncgx_check_native!%s!%s" % (metric_def['command'], args)
        log.debug('     %s' % c_line)
        return c_line
    else:
        log.warning("Unable to find any arguments for ")



