import logging

log = logging.getLogger('ncgx')


def template_function(line, base_config, context):
    log.debug('  tfunction(test)')
    assert isinstance(base_config, dict)

    log.debug(line)
    log.debug(context)

    return 'test_success\n    test_success_2\n    test_success_3\n'

