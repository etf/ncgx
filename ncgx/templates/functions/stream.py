import logging
try:
    import StringIO as io
except ImportError:
    import io

log = logging.getLogger('ncgx')


def template_function(_, base_config, context):
    log.debug('  tfunction(nagios_mq)')
    log.debug('    '+str(context))
    log.debug('    '+getattr(context, '_tags', 'None'))
    _nagios_mq = {
        '_vo': base_config.get('VO', 'None'),
        '_vo_fqan': '/' + '/'.join(context['metric'].split('/')[1:]),
        '_server': base_config.get('NAGIOS_HOST', 'localhost'),
        '_service_uri': context.get('hostname', 'localhost'),
        '_metric_name': context.get('metric', 'None'),
        '_tags': context.get('_tags', 'None'),
        '_unique_tag': context.get('_unique_tag', 'None'),
    }
    if _nagios_mq['_vo_fqan'] == '/':
        _nagios_mq['_vo_fqan'] = '/'+_nagios_mq['_vo']

    out = io.StringIO()
    for k, v in _nagios_mq.items():
        out.write("    %-22s %s\n" % (k, v))
    return out.getvalue()
