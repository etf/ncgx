import logging

log = logging.getLogger('ncgx')


def template_function(line, base_config, context):
    if 'DEFINE_HOSTGROUPS' in base_config.keys() and not base_config['DEFINE_HOSTGROUPS']:
        return " "
    if "host_groups" in context.keys() and context['host_groups']:
        return "hostgroups       "+",".join(context['host_groups'])
    else:
        return " "
