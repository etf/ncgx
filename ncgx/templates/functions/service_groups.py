import logging

log = logging.getLogger('ncgx')


def template_function(line, base_config, context):
    if 'DEFINE_SERVICEGROUPS' in base_config.keys() and not base_config['DEFINE_SERVICEGROUPS']:
        return " "
    if 'service_groups' in context.keys() and context['service_groups']:
        return "servicegroups           "+",".join(context['service_groups'])
    else:
        return " "
