import logging

log = logging.getLogger('ncgx')


def template_function(line, base_config, context):
    if 'doc' in context.keys():
        return "notes           %s" % context['doc']
    elif 'docurl' in context.keys():
        return "notes_url       %s" % context['docurl']
    else:
        return " "
