import logging
try:
    import StringIO as io
except ImportError:
    import io

log = logging.getLogger('ncgx')


def template_function(line, base_config, context):
    out = io.StringIO()
    if 'DEFAULT_CONTACT_GROUPS' in base_config.keys():
        out.write("    %-22s %s\n" % ('contact_groups', base_config.get('DEFAULT_CONTACT_GROUPS', 'msg-contacts')))
    if 'contacts' in context.keys():
        out.write("    %-22s %s\n" % ('contacts', ",".join(context['contacts'])))
    if 'contacts' in context.keys() and 'notification_options' in context.keys():
        out.write("    %-22s %s\n" % ('notification_options', context['notification_options']))
    if 'contacts' in context.keys() and 'notification_interval' in context.keys():
        out.write("    %-22s %s\n" % ('notification_interval', context['notification_interval']))
    return out.getvalue()

