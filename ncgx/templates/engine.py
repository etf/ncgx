import sys
import os
import logging
import glob
import re
import traceback
import json
import socket
import pkgutil
import tempfile
import shutil
import subprocess
from future.utils import string_types
try:
    import StringIO as io
except ImportError:
    import io

from ncgx import settings
from ncgx.structures import Groups, Contacts
from ncgx.config import base_config
from ncgx.network import get_fqdn
import ncgx.templates.functions

log = logging.getLogger('ncgx')


def process_template(template, context={}):
    out = template[:]

    def replace_vars(match_obj):
        match = match_obj.group(1)
        default = match_obj.group(2)
        if match in base_config.keys():
            return str(base_config[match])
        elif match in context.keys():
            return str(context[match])
        elif default:
            return str(default[1:])
        else:
            log.error("Failed to replace variable {0}, no matching context found".format(match))
            return str(match)

    def func_closure(in_line):
        def replace_func(match_obj):
            match = match_obj.group(1)
            try:
                # change to importlib.import_module for 2.7+
                default_pkg = os.path.dirname(ncgx.templates.functions.__file__)
                if match in [name for _, name, _ in pkgutil.iter_modules([default_pkg])]:
                    mod = __import__("ncgx.templates.functions.%s" % match, None, None, [match])
                else:
                    mod = __import__("x_functions.%s" % match, None, None, [match])
            except ImportError as e:
                log.error("Exception %s while loading t_function %s" % (e, match))
                sys.exit(-1)
            try:
                func_out = mod.template_function(in_line, base_config, context)
            except Exception as e:
                log.error("Exception was thrown while running t_function %s" % match)
                log.error("%s" % e)
                log.error("Stack trace:")
                traceback.print_exc(file=sys.stderr)
                sys.exit(-1)

            if not func_out:
                log.error("Template function %s returned no output for args (%s, %s, %s)" %
                          (match, line, base_config, context))
            return func_out

        return replace_func

    for i, line in enumerate(template):
        assert isinstance(line, str)

        if '::t_function' in line:
            out[i] = re.sub(r'::t_function\(\b([A-Za-z0-9_]*)\b\)', func_closure(line), line)
        elif '::' in line:
            # out[i] = re.sub(r'::\b([A-Za-z0-9_]*)\b', replace_vars, line)
            log.debug(line)
            out[i] = re.sub(r'::\b([A-Za-z0-9_]*)(\|[A-Za-z0-9]*)?\b', replace_vars, line)

    return out


class TemplateException(Exception):
    def __init__(self, expr, msg):
        self.expr = expr
        self.msg = msg


class TemplateEngine(object):
    def __init__(self, inventory_json, ipv6=False):
        log.info("Loading inventory ...")

        self._inventory = None
        self._hosts = None
        self._generics = os.path.join(settings.TEMPLATE_PATH, "generic")
        self._inventory = json.loads(inventory_json)
        self._ipv6 = ipv6
        self._h_groups = Groups("host_groups")
        self._s_groups = set()
        self._contacts = Contacts()

        host_template_path = os.path.join(settings.TEMPLATE_PATH, "host_template.tpl")
        service_template_path = os.path.join(settings.TEMPLATE_PATH, "service_template.tpl")
        hg_template_path = os.path.join(settings.TEMPLATE_PATH, "host_groups_template.tpl")
        sg_template_path = os.path.join(settings.TEMPLATE_PATH, "service_groups_template.tpl")
        c_template_path = os.path.join(settings.TEMPLATE_PATH, "contacts.tpl")

        self._tmp_dir = tempfile.mkdtemp()
        self._tmp_npath = os.path.join(self._tmp_dir, "conf.d")
        try:
            os.mkdir(self._tmp_npath)
        except OSError as e:
            log.exception(e)
            sys.exit(-1)

        if "NAGIOS_PATH" in base_config.keys():
            self._nagios_path = base_config["NAGIOS_PATH"]

        # if "hosts" in inventory.keys():
        #     self._hosts = inventory["hosts"]

        if not self._inventory:
            log.error("Inventory empty")
            sys.exit(-1)

        if not (os.path.exists(host_template_path) or os.path.exists(service_template_path) or
                os.path.exists(self._generics)):
            log.error("Failed to load templates from %s" % settings.TEMPLATE_PATH)
            sys.exit(-1)

        with open(host_template_path, "r") as htp:
            self._host_template = htp.readlines()

        with open(service_template_path, "r") as stp:
            self._service_template = stp.readlines()

        if 'host_groups' in self._inventory.keys():
            for h, p in self._inventory['host_groups'].items():
                self._h_groups.add(h, p)
            del(self._inventory['host_groups'])
            if not os.path.exists(hg_template_path):
                log.error("Failed to load host groups template from %s" % settings.TEMPLATE_PATH)
            with open(hg_template_path, "r") as hgtp:
                self._hg_template = hgtp.readlines()

        if 'service_groups' in self._inventory.keys():
            for sg in self._inventory['service_groups'].keys():
                self._s_groups.add(sg)
            del(self._inventory['service_groups'])
            if not os.path.exists(sg_template_path):
                log.error("Failed to load service groups template from %s" % settings.TEMPLATE_PATH)
            with open(sg_template_path, "r") as sgtp:
                self._sg_template = sgtp.readlines()

        if 'contacts' in self._inventory.keys():
            for e in self._inventory['contacts']:
                self._contacts.add(e[1], metric_expr=e[2], host_expr=e[3], options=e[4])
            del(self._inventory['contacts'])
            if not os.path.exists(c_template_path):
                log.error("Failed to load contacts template from %s" % settings.TEMPLATE_PATH)
            with open(c_template_path, "r") as ctp:
                self._c_template = ctp.readlines()

    def _copy_generics(self):
        generic_files = [f for f in glob.glob(self._generics + "/*.cfg")
                         if os.path.isfile(os.path.join(self._generics, f))]

        for t_file in generic_files:
            # resolve global vars
            try:
                with open(t_file, "r") as tf:
                    with open(os.path.join(self._tmp_npath,
                                           os.path.basename(t_file)), "w") as new_tf:
                        template = tf.readlines()
                        log.info("   %s" % t_file)
                        n_template = process_template(template)
                        new_tf.writelines(n_template)
            except Exception as e:
                log.error("Failed to process generic template {0}".format(t_file))
                log.error("Exception was %s" % e)
                sys.exit(-1)

    def debug_host(self, host):
        if host not in self._inventory.keys():
            log.error("Failed to find %s in the inventory" % host)
            sys.exit(-1)

        out = io.StringIO()
        out.write("\nGenerated Nagios configuration:\n")
        ip, hdef = self._get_host_def(host)
        out.writelines(hdef)
        for metric in self._inventory[host].keys():
            sdef = self._get_srv_def(host, ip, metric)
            out.writelines(sdef)

        log.info(out.getvalue())
        sys.exit(0)

    def _get_host_def(self, host):
        try:
            ip = get_fqdn(host, ipv6=self._ipv6)
            log.info("Found host: %s (%s)" % (host, ip))
        except (socket.gaierror, socket.herror, socket.error) as e:
            log.warning("Failed to resolve host %s (%s)" % (host, e))
            if 'DNS_RESOLVE' in base_config.keys() and not base_config['DNS_RESOLVE']:
                log.info("Found host: %s (127.0.0.1)" % host)
                ip = "127.0.0.1"
            else:
                return None, None
        log.info("Generating configuration:")
        log.info("  host: %s " % host)
        # host definition
        _context = {'hostname': host, 'ip': ip}
        # add host groups
        if self._h_groups:
            hgs = self._h_groups.match(host)
            if hgs:
                _context['host_groups'] = hgs
        # add contacts
        if self._contacts:
            cc, options = self._contacts.get_contacts(host, None)
            if cc:
                _context['contacts'] = cc
            if options:
                _context.update(options)
        if self._ipv6:
            _context['host_check_command'] = 'ncgx_check_host_alive_ipv6'
        else:
            _context['host_check_command'] = 'ncgx_check_host_alive'

        return ip, process_template(self._host_template, context=_context)

    def _get_srv_def(self, host, ip, metric):
        log.info("     metric: %s" % metric)
        _context = {'hostname': host, 'ip': ip, 'metric': metric}
        _context.update(self._inventory[host][metric])
        if self._contacts:
            cc, options = self._contacts.get_contacts(host, metric)
            if cc:
                _context['contacts'] = cc
            if options:
                _context.update(options)
        return process_template(self._service_template, context=_context)

    def generate_nagios_config(self, skip_host_def=False, skip_hosts=None, safe=False, ipv6=False):
        log.info("Copying generic templates ...")
        self._copy_generics()

        if skip_hosts and not isinstance(skip_hosts, string_types):
            skip_hosts = skip_hosts.split(',')

        # define host groups
        if 'DEFINE_HOSTGROUPS' in base_config.keys() and base_config['DEFINE_HOSTGROUPS']:
            with open(os.path.join(self._tmp_npath, "host_groups.cfg"), "w") as hg:
                for _hg_name in self._h_groups.get_groups():
                    hg_def = process_template(self._hg_template, context={'hg': _hg_name,
                                                                          'members': self._h_groups.get_group_items(
                                                                              _hg_name)})
                    hg.writelines(hg_def)

        # define service groups
        if 'DEFINE_SERVICEGROUPS' in base_config.keys() and base_config['DEFINE_SERVICEGROUPS']:
            with open(os.path.join(self._tmp_npath, "service_groups.cfg"), "w") as sg:
                for _sg_name in self._s_groups:
                    sg_def = process_template(self._sg_template, context={'sg': _sg_name})
                    sg.writelines(sg_def)

        # define contacts
        if 'DEFINE_CONTACTS' in base_config.keys() and base_config['DEFINE_CONTACTS']:
            with open(os.path.join(self._tmp_npath, "contacts.cfg"), "w") as cc:
                for contact, email in self._contacts.get_all_contacts():
                    cc_def = process_template(self._c_template, context={'contact_name': contact,
                                                                         'contact_email': email})
                    cc.writelines(cc_def)

        for host in self._inventory.keys():
            ip, hdef = self._get_host_def(host)
            if not hdef or not ip:
                log.warning("  Skipping %s configuration (failed DNS resolve)" % host)
                continue
            with open(os.path.join(self._tmp_npath, host + '.cfg'), "w") as nc:
                # host definition
                if skip_host_def or (skip_hosts and host in skip_hosts):
                    log.warning("  Skipping %s host definitions" % host)
                else:
                    nc.write("#\n# Generated: %s \n# \n" % host)
                    nc.writelines(hdef)
                    nc.write("\n")
                for metric in self._inventory[host].keys():
                    # service definition
                    sdef = self._get_srv_def(host, ip, metric)
                    nc.write("#\n# Generated: %s %s \n# \n" % (host, metric))
                    nc.writelines(sdef)
                    log.debug("Wrote %s" % nc.name)

        nagios_cfg = os.path.join(self._tmp_npath, "nagios.cfg")
        if safe:
            if os.path.isfile(nagios_cfg):
                shutil.move(nagios_cfg, self._tmp_dir)
                nagios_cfg = os.path.join(self._tmp_dir, "nagios.cfg")
                with open(nagios_cfg, 'a') as ncf:
                    ncf.write("cfg_dir=%s\n" % self._tmp_npath)
            else:
                log.warning("Unable to validate, nagios.cfg not found")
                sys.exit(-1)

            # change ownership of the temp directory (avoids Unable to open config file)
            # didn't work - some issue with os.chown not behaving like chown -R in shell
            # workaround is to set nagios_user/nagios_group to executing user/group
            # the other option is to getlogin() and change nagios_user in nagios.cfg
            #if 'NAGIOS_USER' in base_config.keys():
            #    u_id = pwd.getpwnam(base_config['NAGIOS_USER'])
            #    if not u_id:
            #        log.warning("Unable to determine user id for %s" % base_config['NAGIOS_USER'])
            #    else:
            #        for root, dirs, files in os.walk(self._tmp_dir):
            #            for d in dirs:
            #                os.chown(os.path.join(self._tmp_dir, d), u_id, None)
            #            for f in files:
            #                os.chown(os.path.join(self._tmp_dir, f), u_id, None)

            ng_sane = subprocess.call("nagios -v %s" % nagios_cfg, stdout=sys.stdout, stderr=sys.stderr,
                                      shell=True)
            if ng_sane != 0:
                log.warning("Generated configuration is invalid, keeping the existing configuration")
                log.warning("Failed")
                #shutil.rmtree(self._tmp_dir)
                sys.exit(-1)
        elif os.path.isfile(nagios_cfg):
                os.remove(nagios_cfg)

        try:
            shutil.rmtree(self._nagios_path)
            shutil.copytree(self._tmp_npath, self._nagios_path)
        except Exception as e:
            log.exception(e)
            sys.exit(-1)
