import logging
import datetime
import sys
import glob
import os

import ncgx
from ncgx import settings
from ncgx.config import base_config
from ncgx.plugins import load_plugins
from ncgx.inventory import InventoryCompiler
from ncgx.templates.engine import TemplateEngine

log = logging.getLogger('ncgx')


def main(args):
    header = list()
    header.append("ncgx v.%s: %s" % (ncgx.__version__, datetime.datetime.now()))
    header.append("Using config dir: %s" % settings.CONFIG_PATH)
    header.append("Using local config sources:")
    cf_files = [f for f in glob.glob(settings.LOCAL_CONFIG_PATH + "/*.%s" % settings.CONFIG_EXT)
                if os.path.isfile(os.path.join(settings.LOCAL_CONFIG_PATH, f))]
    for cf_file in cf_files:
        header.append("    %s" % cf_file)
    l_max = len(max(header, key=lambda x: len(x)))
    log.info('*' * (l_max + 4))
    for l in header:
        log.info('* {0:<{1}s} *'.format(l, l_max))
    log.info('*' * (l_max + 4))

    if not args.inventory:
        if not args.skip_plugins:
            # Load plugins
            load_plugins()
        if args.plugins_only:
            log.info("Done")
            sys.exit(0)

        # Compile inventory
        ic = InventoryCompiler()
        ic.compile()
        if args.simulate:
            ic.simulate(args.host, args.metric)
            sys.exit(0)
        if args.tag:
            ic.debug_tag(args.tag)
        if args.explain:
            ic.explain(args.metric, host=args.host)
        inventory = ic.jsonify()
        ic.serialize()
        if args.inventory_only:
            log.info("Done")
            sys.exit(0)
    else:
        with open(args.inventory, 'r') as inv:
            inventory = inv.read()

    # Generate Nagios configuration
    te = TemplateEngine(inventory, ipv6=args.ipv6)
    if args.nagios and args.host:
        te.debug_host(args.host)
    else:
        disable_hd = False
        if ("DISABLE_HOST_DEF" in base_config.keys() and base_config['DISABLE_HOST_DEF']) or args.no_host_def:
            disable_hd = True
        if "SKIP_HOST_DEF" in base_config.keys() and not args.skip_hosts:
            args.skip_hosts = base_config["SKIP_HOST_DEF"]
        te.generate_nagios_config(skip_host_def=disable_hd, skip_hosts=args.skip_hosts, safe=args.safe)
    log.info("Done")


