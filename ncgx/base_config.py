import os
import sys
import logging

from ncgx import settings

__all__ = ['base_config']

log = logging.getLogger('ncgx')

if not os.path.exists(settings.CONFIG_PATH):
    log.error("Default config path access error (%s)" % settings.CONFIG_PATH)
    sys.exit(1)

_bcf = os.path.join(settings.CONFIG_PATH, "ncgx.%s" % settings.CONFIG_EXT)

if not os.path.exists(_bcf):
    log.error("Unable to read metrics from %s" % _bcf)
    sys.exit(1)

base_config = {}
settings.ncxg_exec(_bcf, {}, base_config)
log.debug("base config: %s" % base_config)
