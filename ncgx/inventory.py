import logging
import os
import time
import sys
import json
import collections
import copy
from future.utils import string_types

import ncgx.config
from ncgx.scripts import load_scripts
from ncgx.structures import Metrics, Checks, Hosts, Groups, Contacts, deep_update
from ncgx import settings

log = logging.getLogger('ncgx')


class InventoryCompiler(object):
    def __init__(self):
        self._base_config = None
        self._hosts = Hosts()
        self._checks = Checks()
        self._metrics = Metrics()
        self._h_groups = Groups("host_groups")
        self._s_groups = Groups("service_groups")
        self._contacts = Contacts()
        self._inventory = collections.defaultdict(dict)

        # load base config
        # load base metrics - load custom metrics - process overrides (auto); process inheritance
        # load all hosts (final hosts)
        # load all checks
        # expand metrics with checks params (final metrics; only overrides)

        # load base config
        self._base_config = ncgx.config.base_config

        # load base metrics
        if ncgx.config.metrics:
            for m, m_def in ncgx.config.metrics['metrics'].items():
                self._metrics.add_metric(m, m_def)

        # load metrics.d
        for m_dict in ncgx.config.metrics_d:
            for m, m_def in m_dict['metrics'].items():
                self._metrics.add_metric(m, m_def)

        # load local metrics
        from ncgx.config.local import local_config
        for entry in local_config:
            if 'metrics' in entry.keys():
                for m, m_def in entry['metrics'].items():
                    self._metrics.add_metric(m, m_def)

            if 'hosts' in entry.keys():
                for h, tag in entry['hosts'].items():
                    self._hosts.add(h, tags=tag)

            if 'host_groups' in entry.keys():
                for hg in entry['host_groups']:
                    self._h_groups.add(hg[0], hg[1])

            if 'service_groups' in entry.keys():
                for sg in entry['service_groups']:
                    self._s_groups.add(sg[0], sg[1])

            if 'contacts' in entry.keys():
                for c in entry['contacts']:
                    self._contacts.add(c[0], metric_expr=c[1], host_expr=c[2], options=c[3])

            if 'checks' in entry.keys():
                for check in entry['checks']:
                    check_params = {}
                    if len(check) == 3:
                        check_params = check[2]
                    th = check[0]
                    if isinstance(th, string_types) and not th.startswith('tag:'):
                        self._checks.add(check[1], hosts=[th], params=check_params)
                    elif isinstance(th, string_types):
                        self._checks.add(check[1], tags=[th[4:]], params=check_params)
                    else:
                        th = [e.replace("tag:", "") for e in th]
                        self._checks.add(check[1], tags=th, params=check_params)
                        # mixing tags and hosts in one check stmt is not possible

        log.info("Configuration loaded (%d metrics, %d hosts, %d checks)" %
                 (len(self._metrics), len(self._hosts), len(self._checks)))

    def simulate(self, host, metric):
        log.setLevel(logging.INFO)
        if host not in self._inventory.keys():
            log.error("Failed to find the specified host %s in the inventory" % host)
            sys.exit(-1)
        if metric not in self._inventory[host].keys():
            log.error("Failed to find the specified metric %s in the inventory" % metric)
            sys.exit(-1)

        metric_def = self._inventory[host][metric]
        args = [(k, v) for k, v in metric_def['args'].items()]
        log.info("su nagios -l -c \'%s %s\'" % (metric_def['command'], " ".join('%s %s' % tup for tup in args)))
        sys.exit(0)

    def explain(self, metric, host=None):
        if not self._metrics.get_metric_def(metric):
            log.error("Failed to find the specified metric %s in the inventory" % metric)
            sys.exit(-1)

        metric_def = self._metrics.get_metric_def(metric)
        log.info("Metric definition for %s" % metric)
        for k, v in metric_def.items():
            log.info("  %s : %s" % (k, v))

        deps = self._metrics.get_dependency(metric)
        if deps:
            log.info("\nDependencies:")
            log.info(deps)

        if host:
            for k,v in self._inventory[host][metric].items():
                log.info("  %s : %s" % (k, v))
        sys.exit(0)

    def debug_tag(self, tag):
        log.setLevel(logging.INFO)
        hosts = self._hosts.get_hosts(tag)
        for host in hosts:
            log.info(host)
        sys.exit(0)

    def _auto_add_metrics(self, check):
        metric = check[1]
        metric_def = self._metrics.get_metric_def(metric)
        if not metric_def and len(check) > 2 and 'extends' in check[2]:
            log.info("Auto-added metric %s (extending %s)" % (metric, check[2]['extends']))
            self._metrics.add_metric(metric, copy.copy(check[2]))
            return

        if not metric_def and 'AUTO_PASSIVE' in ncgx.config.base_config.keys():
            log.info("Auto-added passive metric %s" % metric)
            self._metrics.add_metric(metric, {'passive': True})
            return

    def _metric_consistency(self, check):
        metric = check[1]
        metric_def = self._metrics.get_metric_def(metric)

        if not metric_def and 'COMMAND_OVERRIDE' not in ncgx.config.base_config.keys():
            log.error("Unable to find metric definition for metric %s" % metric)
            sys.exit(1)

    def _dump_inventory(self):
        for h in self._inventory.keys():
            log.debug("%s" % h)
            for m in self._inventory[h].keys():
                m_def = self._inventory[h][m]
                if isinstance(m_def, collections.Mapping):
                    log.debug("    %s" % m)
                    for k, v in m_def.items():
                        log.debug("        %s -> %s" % (k, v))
                else:
                    log.debug("    %s -> %s" % (m, m_def))

    def compile(self):

        # auto add metric def for shortcuts (passive checks, checks with direct inheritance)
        for check in self._checks.get_host_checks():
            self._auto_add_metrics(check)

        for check in self._checks.get_tagged_checks():
            self._auto_add_metrics(check)

        for check in self._checks.get_and_tagged_checks():
            self._auto_add_metrics(check)

        # process metric templates (inheritance/extends)
        self._metrics.apply_templates()

        # compile all checks
        all_checks = list()
        host_checks = self._checks.get_host_checks()
        all_checks.extend(host_checks)

        for check in host_checks:
            # check for missing metric defs
            self._metric_consistency(check)

        tag_checks = self._checks.get_tagged_checks()
        # resolve tags in checks
        for check in tag_checks:
            # check for missing metric defs
            self._metric_consistency(check)

            tag = check[0]
            metric = check[1]
            params = check[2] if len(check) == 3 else dict()
            metric_def = self._metrics.get_metric_def(metric)

            # tag_checks
            if isinstance(tag, string_types):
                hosts = self._resolve_tag(metric, metric_def, tag)
                all_checks.extend([[h, metric, params] for h in hosts])
            else:
                # unreachable ? since in get_tagged_checks, tag can never be a list
                for tg in tag:
                    hosts = self._resolve_tag(metric, metric_def, tg)
                    all_checks.extend([[h, metric, params] for h in hosts])

        # resolve AND tags in checks
        for check in self._checks.get_and_tagged_checks():
            self._metric_consistency(check)

            tags = check[0]
            metric = check[1]
            params = check[2] if len(check) == 3 else dict()
            metric_def = self._metrics.get_metric_def(metric)

            hosts_list = list()
            for t in tags:
                hosts_list.append(self._resolve_tag(metric, metric_def, t))
            hosts = set.intersection(*hosts_list)
            all_checks.extend([[h, metric, params] for h in hosts])

        # resolve variables in metric definitions
        self._metrics.resolve_vars(self._base_config)

        # process attribute overrides
        if 'ATTRIBUTE_OVERRIDE' in ncgx.config.base_config.keys():
            assert not isinstance(ncgx.config.base_config['ATTRIBUTE_OVERRIDE'], string_types)
            for entry in ncgx.config.base_config['ATTRIBUTE_OVERRIDE']:
                log.info("Applying override: %s (%s)" % (entry[0], entry[1]))
                assert isinstance(entry[1], collections.Mapping)
                self._metrics.override(entry[0], entry[1])

        # create initial inventory (host1..n -> metric1..n -> metric_def1..n)
        for check in all_checks:
            host = check[0]
            metric = check[1]
            params = check[2] if len(check) == 3 else dict()
            # add metric definitions (check consistency)
            m_def = self._metrics.get_metric_def(metric)
            self._inventory[host][metric] = copy.deepcopy(m_def)
            # apply param overrides (if any)
            if params:
                deep_update(self._inventory[host][metric], params)
        log.debug('Initial inventory:')
        if log.getEffectiveLevel() == logging.DEBUG:
            self._dump_inventory()

        # resolve dependencies (add additional metrics to inventory)
        inv_index = list(self._inventory.items())
        self._metrics.build_dependency_tree()
        for host, ms in inv_index:
            for metric in list(ms.keys()):
                dep_metrics = self._metrics.get_dependency(metric)

                for m in dep_metrics:
                    if m == metric or m in self._inventory[host].keys():
                        # each metric depends on itself
                        # skip metrics already resolved (already in the inventory)
                        continue
                    elif 'target' in self._metrics.get_metric_def(m):
                        # metrics for other hosts
                        h = self._metrics.get_metric_def(m)['target']
                        self._inventory[h][m] = self._metrics.get_metric_def(m)
                    else:
                        self._inventory[host][m] = copy.deepcopy(self._metrics.get_metric_def(m))
        s = len([m for h in self._inventory.keys() for m in self._inventory[h]])
        log.info("Inventory compiled (%d hosts, %d services)" % (len(self._inventory), s))
        if log.getEffectiveLevel() == logging.DEBUG:
            self._dump_inventory()

        # call inventory post-processing scripts
        load_scripts(self._inventory, self._hosts)

        # add service groups to metric definitions
        for h in self._inventory.keys():
            for m in self._inventory[h].keys():
                m_def = self._inventory[h][m]
                m_def['service_groups'] = list()
                sg = self._s_groups.match(m)
                if sg:
                    m_def['service_groups'].extend(sg)

        self._inventory['service_groups'] = self._s_groups.copy()

        # resolve tags in host groups
        # add host-groups to inventory
        tags = self._hosts.get_tags()
        for tag in tags:
            for gr in self._h_groups.exact_match(tag):
                self._h_groups.add(gr, self._hosts.get_hosts(tag))
                self._h_groups.remove(gr, tag)
        self._inventory['host_groups'] = self._h_groups.copy()

        # resolve host tags in contacts
        # add contacts to inventory
        self._contacts.expand_tags(self._hosts)
        self._contacts.expand_hostgroups(self._h_groups)
        self._inventory['contacts'] = self._contacts.copy()

        # todo: add metadata structures to inventory (checks, hosts, tags, metrics defs, contacts)
        # todo: add host-based inventory to natively support host_groups and host notifications

        log.debug("Final inventory")
        if log.getEffectiveLevel() == logging.DEBUG:
            self._dump_inventory()
        # done

    def _resolve_tag(self, metric, metric_def, tag):
        # add metadata
        if '_tags' in metric_def.keys():
            self._metrics.add_metric(metric, {'_tags': metric_def['_tags'] + ' ' + tag})
        else:
            self._metrics.add_metric(metric, {'_tags': tag})
        # resolve
        hosts = self._hosts.get_hosts(tag)
        return hosts

    def jsonify(self):
        return json.dumps(self._inventory)

    def serialize(self, fname=None):
        if not fname:
            fname = 'inventory_%d.json' % time.time()
            fname = os.path.join(settings.INVENTORY_PATH, fname)

        with open(fname, 'w') as ifile:
            ifile.write(self.jsonify())
