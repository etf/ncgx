import logging
import requests
import xml.etree.ElementTree as ET

from ncgx.inventory import Hosts, Checks

log = logging.getLogger('ncgx')


def http_get(feed):
    r = requests.get(feed)
    if r.status_code != 200:
        log.error("Failed to retrieve the feed at %s" % feed)
    return r.text


def get_services(vo_feed):
    response = http_get(vo_feed)

    if not response:
        log.error("Empty response received while reading the feed")
        return None

    tree = ET.fromstring(response)
    services = list()
    atp_sites = tree.findall('atp_site')
    for s in atp_sites:
        services.extend([(x.attrib['hostname'].strip(), x.attrib['flavour'].strip())
                         for x in s.findall('service')])
    log.info("Services in the feed: %d" % len(services))
    return services


def run(feed):
    log.info("Processing vo feed: %s" % feed)

    services = get_services(feed)
    h = Hosts()
    for service in services:
        h.add(service[0], tags=[service[1]])
    h.serialize()

    c = Checks()
    c.add("emi.cream.CREAMCE-DirectJobSubmit-/lhcb/Role=production", tags=["CREAM-CE"])
    c.add("emi.cream.glexec.CREAMCE-DirectJobSubmit-/lhcb/Role=pilot", tags=["CREAM-CE"])
    c.add("org.sam.ARC-JobSubmit-/lhcb/Role=production", tags=["ARC-CE"])
    c.add("org.sam.ARC-JobSubmit-/lhcb/Role=pilot", tags=["ARC-CE"])
    c.add("org.lhcb.SRM-VOLs-/lhcb/Role=production", tags=["SRMv2"])
    c.serialize()
