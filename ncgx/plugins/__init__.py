import logging
import sys
import traceback
import time
import pkgutil
import os.path
import signal

from ncgx.config import base_config
import ncgx.settings
import ncgx.plugins

log = logging.getLogger('ncgx')


class TimeoutError(Exception):
    pass


def _handle_timeout(signum, frame):
    raise TimeoutError("Plugin execution timed out after %d seconds" % ncgx.settings.PLUGIN_TIMEOUT)


def load_plugins():
    if 'PLUGINS' not in base_config.keys():
        return
    log.info("Configured plugins: %s" % base_config['PLUGINS'])

    for plugin in base_config['PLUGINS'].keys():
        try:
            default_pkg = os.path.dirname(ncgx.plugins.__file__)
            # change to importlib.import_module for 2.7+
            # check if plugin in default plugins otherwise lookup user plugins
            if plugin in [name for _, name, _ in pkgutil.iter_modules([default_pkg])]:
                mod = __import__("ncgx.plugins.%s" % plugin, None, None, base_config['PLUGINS'].keys())
            else:
                mod = __import__("x_plugins.%s" % plugin, None, None, base_config['PLUGINS'].keys())
        except ImportError as e:
            log.error("Exception %s while loading plugin %s" % (e, plugin))
            sys.exit(-1)
        except:
            log.error("Exception while importing plugin")
            e = sys.exc_info()[0]
            log.exception(e)
            sys.exit(2)

        try:
            log.debug("Calling plugin: %s(%s)" % (plugin, base_config['PLUGINS'][plugin]))
            signal.signal(signal.SIGALRM, _handle_timeout)
            signal.alarm(ncgx.settings.PLUGIN_TIMEOUT)
            s_time = time.time()
            if base_config['PLUGINS'][plugin]:
                mod.run(**base_config['PLUGINS'][plugin])
            else:
                mod.run()
            e_time = time.time()
            log.info("Plugin: %s, runtime: %.3fs" % (plugin, e_time-s_time))
        except Exception as e:
            log.error("Exception was thrown while running plugin %s" % plugin)
            log.error("%s" % e)
            log.error("Stack trace:")
            traceback.print_exc(file=sys.stderr)
            sys.exit(-1)
        except:
            log.error("Exception while calling plugin")
            e = sys.exc_info()[0]
            log.exception(e)
            sys.exit(2)
        finally:
            signal.alarm(0)
