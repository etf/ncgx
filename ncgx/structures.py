import os
import logging
import sys
import re
import json
import copy
import inspect
from collections import defaultdict, Mapping
from future.utils import string_types

from ncgx import settings
import ncgx.config

log = logging.getLogger('ncgx')


def depth_first_search(graph, start):
    path = set()
    stack = [start]
    while stack:
        node = stack.pop()
        if node not in path:
            path.add(node)
            stack.extend(graph[node] - path)
    return path


def deep_update(source, overrides):
    for key, value in overrides.items():
        if isinstance(value, Mapping) and value:
            returned = deep_update(source.get(key, {}), value)
            source[key] = returned
        else:
            source[key] = overrides[key]
    return source


def deep_resub(source, regex, function):
    assert hasattr(function, '__call__')

    for key, value in source.items():
        if isinstance(value, Mapping) and value:
            deep_resub(value, regex, function)
        elif isinstance(value, string_types) and '::' in value:
            source[key] = re.sub(regex, function, str(value))


class Hosts(object):
    def __init__(self):
        self._hosts = defaultdict(list)

    def serialize(self, fname=None):
        mod = None
        if not fname:
            frm = inspect.stack()[1]
            mod = inspect.getmodule(frm[0]).__name__
            fname = '{0:s}_hosts_generated.{1:s}'.format(mod.replace(".", "_"), settings.CONFIG_EXT)
            fname = os.path.join(settings.LOCAL_CONFIG_PATH, fname)

        with open(fname, 'w') as sf:
            sf.write("hosts = {\n")
            for k, v in self._hosts.items():
                if 'AUTO_TAG_HOSTS' in ncgx.config.base_config.keys() and \
                                       ncgx.config.base_config['AUTO_TAG_HOSTS']:
                    if 'ALL' not in v:
                        v.append('ALL')
                    if mod:
                        v.append(mod.replace(".", "_"))
                sf.write("          '%s' : %s,\n" % (k, v))
            sf.write("}\n")

    def add(self, host, tags=()):
        assert tags
        for tag in tags:
            if tag not in self._hosts[host]:
                self._hosts[host].append(tag)

    def tag(self, tag, hosts=()):
        for host in hosts:
            if tag not in self._hosts[host]:
                self._hosts[host].append(tag)

    def get_hosts(self, tag):
        hosts = set()
        for host in self._hosts.keys():
            if tag in self._hosts[host]:
                hosts.add(host)
        return hosts

    def get_all_hosts(self):
        return set([h for h in self._hosts.keys()])

    def get_tags(self, host=None):
        if host and host in self._hosts.keys():
            return set(self._hosts[host])
        else:
            tags = set()
            for host in self._hosts.keys():
                for tag in self._hosts[host]:
                    tags.add(tag)
        return tags

    def __str__(self):
        return "Hosts %s" % self._hosts

    def jsonify(self):
        return json.dumps(self._hosts)

    def __len__(self):
        return len(self._hosts)


class Checks(object):
    def __init__(self):
        self._tag_checks = []
        self._host_checks = []
        self._and_tag_checks = []

    def get_tagged_checks(self):
        return self._tag_checks

    def get_and_tagged_checks(self):
        return self._and_tag_checks

    def get_host_checks(self):
        return self._host_checks

    def add(self, metric, hosts=(), tags=(), params={}):
        assert metric and (hosts or tags)
        assert not isinstance(hosts, string_types)
        assert not isinstance(tags, string_types)

        if params:
            for host in hosts:
                self._host_checks.append([host, metric, params])
            if '&&' in tags:
                assert len(tags) > 2
                tags.remove('&&')
                self._and_tag_checks.append([tags, metric, params])
                return
            for tag in tags:
                self._tag_checks.append([tag, metric, params])
        else:
            for host in hosts:
                self._host_checks.append([host, metric])
            if '&&' in tags:
                assert len(tags) > 2
                tags.remove('&&')
                self._and_tag_checks.append([tags, metric])
                return
            for tag in tags:
                self._tag_checks.append([tag, metric])

    def add_all(self, metrics=(), hosts=(), tags=()):
        assert metrics and (hosts or tags)
        assert not isinstance(hosts, string_types)
        assert not isinstance(tags, string_types)
        assert not isinstance(metrics, string_types)

        for m in metrics:
            self.add(m, hosts=hosts, tags=tags)

    def serialize(self, fname=None):
        mod = None
        if not fname:
            frm = inspect.stack()[1]
            mod = inspect.getmodule(frm[0]).__name__
            fname = '{0:s}_checks_generated.{1:s}'.format(mod.replace(".", "_"), settings.CONFIG_EXT)
            fname = os.path.join(settings.LOCAL_CONFIG_PATH, fname)

        with open(fname, 'w') as sf:
            sf.write("checks = [\n")
            for at in self._and_tag_checks:
                at = ["tag:"+str(e) for e in at]
                at[0].append('&&')
                sf.write("          %s,\n" % at)
            for te in self._tag_checks:
                te[0] = "tag:"+str(te[0])
                if 'AUTO_TAG_CHECKS' in ncgx.config.base_config.keys() and \
                        ncgx.config.base_config['AUTO_TAG_CHECKS']:
                    if mod:
                        te[0] = ['&&', te[0], "tag:"+str(mod.replace(".", "_"))]
                sf.write("          %s,\n" % te)
            for he in self._host_checks:
                sf.write("          %s,\n" % he)
            sf.write("]\n")

    def __str__(self):
        return 'Checks %s, %s' % (self._host_checks, self._tag_checks)

    def __len__(self):
        return len(self._host_checks) + len(self._tag_checks)

    def jsonify(self):
        return json.dumps(self._tag_checks)


class Metrics(object):
    def __init__(self):
        self._metrics = defaultdict(dict)
        self._dps_tree = dict()

    def add_metric(self, metric, metric_def):
        assert isinstance(metric_def, dict)
        if metric in self._metrics.keys():
            # z = self._metrics[metric].copy()
            deep_update(self._metrics[metric], metric_def)
        else:
            self._metrics[metric] = metric_def

    def apply_templates(self):
        # process templates (assumes parents already loaded)
        parents = list()
        for metric, metric_def in self._metrics.items():
            if 'extends' in metric_def.keys():
                try:
                    parent = self._metrics[metric_def['extends']]
                    if not parent:
                        log.error("Failed to find parent definition of %s, needed by %s" % (metric_def['extends'],
                                                                                            metric))
                        sys.exit(-1)
                    parents.append(metric_def['extends'])
                    # start with a parent, override with child's definitions
                    parent_copy = copy.deepcopy(parent)
                    deep_update(parent_copy, metric_def)
                    self._metrics[metric] = copy.copy(parent_copy)
                except KeyError:
                    log.error("Failed to find parent for %s, metric definition is %s" % (metric, metric_def))
                    sys.exit(-1)
        # remove templates
        for p in set(parents):
            del self._metrics[p]

    def override(self, pattern, metric_def):
        for metric in self._metrics.keys():
            if pattern == 'ALL':
                deep_update(self._metrics[metric], metric_def)
            elif pattern in metric:
                deep_update(self._metrics[metric], metric_def)

    def resolve_vars(self, config):
        # resolve variables and functions
        def replace(match_obj):
            match = match_obj.group(1)
            if match in config.keys():
                return config[match]
            else:
                log.warning("Failed to resolve %s, check base config" % match)
                return match

        deep_resub(self._metrics, r'::\b([A-Za-z0-9_]*)\b', replace)

    def get_metric_def(self, metric):
        return self._metrics[metric]

    def build_dependency_tree(self):
        for k, v in self._metrics.items():
            if 'depends' in v.keys():
                self._dps_tree[k] = set(v['depends'].strip().split(' '))
            else:
                self._dps_tree[k] = set()
        log.debug('Dependency tree: %s' % self._dps_tree)
        log.info('Checking inventory consistency')
        for k in self._dps_tree.keys():
            if k not in self._metrics.keys():
                log.warning("Failed to find metric definition for %s" % k)
            for kdeps in self._dps_tree[k]:
                if kdeps not in self._metrics.keys():
                    log.error("Failed to find metric definition for %s (dependency of %s)" % (kdeps, k))
                    sys.exit(-1)

    def get_dependency(self, metric):
        return depth_first_search(self._dps_tree, metric)

    def __str__(self):
        return "Metrics: %s" % self._metrics

    def __len__(self):
        return len(self._metrics)


class Groups(object):
    def __init__(self, id_name):
        self._groups = defaultdict(set)
        self._id = id_name

    def add(self, group, pattern):
        assert self._id
        if isinstance(pattern, string_types):
            pattern = (pattern, )
        for cp in pattern:
            if cp:
                self._groups[group].add(cp)

    def remove(self, group, item):
        if group in self._groups.keys() and item in self._groups[group]:
            self._groups[group].remove(item)

    def match(self, term):
        _gs = set()
        for g, p in self._groups.items():
            for cp in p:
                if cp in term:
                    _gs.add(g)
        return _gs

    def exact_match(self, term):
        _gs = set()
        for g, p in self._groups.items():
            for cp in p:
                if cp == term:
                    _gs.add(g)
        return _gs

    def get_groups(self):
        return self._groups.keys()

    def get_group_items(self, key):
        if key not in self._groups.keys():
            return None
        return self._groups[key]

    def copy(self):
        _gr = defaultdict(list)
        for k in self._groups.keys():
            _gr[k] = list(self._groups[k])
        return _gr

    def serialize(self, fname=None):
        if not fname:
            frm = inspect.stack()[1]
            mod = inspect.getmodule(frm[0]).__name__
            fname = '{0:s}_{1:s}_generated.{2:s}'.format(mod.replace(".", "_"), self._id, settings.CONFIG_EXT)
            fname = os.path.join(settings.LOCAL_CONFIG_PATH, fname)

        with open(fname, 'w') as sf:
            sf.write("%s = [\n" % self._id)
            for k, v in self._groups.items():
                sf.write('          ["%s", %s],\n' % (k, list(v)))
            sf.write("]\n")


class Contacts(object):
    def __init__(self):
        self._contact_rules = list()

    def add(self, contact, metric_expr="", host_expr="", options={}):
        assert (metric_expr or host_expr)

        contact_name = contact.replace(".", "_").replace("@", "_")
        if not options:
            options = { "notification_options": "c"}
        if isinstance(host_expr, string_types):
            host_expr = (host_expr, )
        if isinstance(metric_expr, string_types):
            metric_expr = (metric_expr, )
        for h in host_expr:
            for m in metric_expr:
                self._contact_rules.append([contact_name, contact, m, h, options])

    def get_all_contacts(self):
        return set([(r[0], r[1]) for r in self._contact_rules])

    def get_contacts(self, host, metric):
        contacts = set()
        opts = dict()
        for rule in self._contact_rules:
            contact_name = rule[0]
            h_expr = rule[3]
            m_expr = rule[2]
            ropts = rule[4] if rule[4] else dict()
            if metric and not h_expr and m_expr in metric:
                contacts.add(contact_name)
                opts.update(ropts)
            if host and not m_expr and h_expr in host:
                contacts.add(contact_name)
                opts.update(ropts)
            if metric and host and m_expr in metric and h_expr in host:
                contacts.add(contact_name)
                opts.update(ropts)
        return contacts, opts

    def expand_hostgroups(self, groups):
        assert isinstance(groups, Groups)

        host_groups = groups.get_groups()

        orig_contacts = copy.copy(self._contact_rules)
        for i in range(0, len(orig_contacts)):
            rule = orig_contacts[i]
            cont = rule[1]
            he = rule[3]
            me = rule[2]
            opts = rule[4]
            if he in host_groups:
                # del(self._contact_rules[i])
                self.add(cont, metric_expr=me, host_expr=groups.get_group_items(he), options=opts)

    def expand_tags(self, hosts_object):
        assert isinstance(hosts_object, Hosts)

        host_tags = hosts_object.get_tags()

        # expand tags and replace the rules that contain tags
        orig_contacts = copy.copy(self._contact_rules)
        for i in range(0, len(orig_contacts)):
            rule = orig_contacts[i]
            cont = rule[1]
            he = rule[3]
            me = rule[2]
            opts = rule[4]
            if he in host_tags:
                #del(self._contact_rules[i])
                self.add(cont, metric_expr=me, host_expr=hosts_object.get_hosts(he), options=opts)

    def serialize(self, fname=None):
        if not fname:
            fname = 'generated_%s.%s' % ('contacts', settings.CONFIG_EXT)
            fname = os.path.join(settings.LOCAL_CONFIG_PATH, fname)

        with open(fname, 'w') as sc:
            sc.write("%s = [\n" % 'contacts')
            for e in self._contact_rules:
                sc.write("%s\n" % e)
            sc.write("]\n")

    def copy(self):
        return copy.deepcopy(self._contact_rules)
