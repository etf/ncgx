import socket
import logging
from future.utils import string_types
import ncgx.config

log = logging.getLogger('ncgx')


def get_fqdn(host, ipv6=False):
    if 'SKIP_DNS_RESOLVE' in ncgx.config.base_config.keys() and \
                    host in ncgx.config.base_config['SKIP_DNS_RESOLVE']:
        assert not isinstance(ncgx.config.base_config['SKIP_DNS_RESOLVE'], string_types)
        return '127.0.0.1'
    host_addr = socket.getaddrinfo(host, 80, 0, 0, socket.IPPROTO_TCP)
    if len(host_addr) > 1:
        log.warning("Multiple IP addresses detected, %s" % host_addr)
    ip6 = list(filter(lambda x: x[0] == socket.AF_INET6, host_addr))
    ip4 = list(filter(lambda x: x[0] == socket.AF_INET, host_addr))
    if ipv6 and ip6:
        return ip6[0][4][0]
    elif not ipv6 and ip4:
        return ip4[0][4][0]
    else:
        return None
