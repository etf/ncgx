import sys


# This is internal fix for missing execfile issue for py2 and py3 (can be removed once on py3)
def ncxg_exec(filepath, globals=None, locals=None):
    if sys.version_info[0] == 2:
        return execfile(filepath, globals, locals)
    if globals is None:
        globals = {}
    globals.update({
        "__file__": filepath,
        "__name__": "__main__",
    })
    with open(filepath, 'rb') as file:
        exec(compile(file.read(), filepath, 'exec'), globals, locals)

#
# These are internal settings for ncgx
# Please don't change anything unless you know what you're doing
#


CONFIG_PATH = "/etc/ncgx"

LOCAL_CONFIG_PATH = CONFIG_PATH+"/conf.d"

METRICS_PATH = CONFIG_PATH+"/metrics.d"

PLUGINS_PATH = "/etc/ncgx/plugins"

INVENTORY_PATH = "/var/cache/ncgx"

TEMPLATE_PATH = "/etc/ncgx/templates"

NAGIOS_CONFIG = "ncgx_generated.cfg"

CONFIG_EXT = "cfg"

PLUGINS_USER_PATH = "/usr/lib/ncgx/"

PLUGIN_TIMEOUT = 300


