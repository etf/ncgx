Summary: Nagios configuration generator
Name: ncgx
Version: 0.1.70
Release: 1%{?dist}
Source: %{name}-%{version}.tar.gz
License: ASL 2.0
Group: Development/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Prefix: %{_prefix}
BuildArch: noarch
%if 0%{?el7}
Requires: python-argparse
%endif
Url: https://gitlab.cern.ch/etf/ncgx
%if 0%{?el7}
BuildRequires: python-setuptools
%else
BuildRequires: python3-setuptools python3-devel /usr/bin/pathfix.py
%endif

%description

Nagios configuration generator


%prep
%setup -n %{name}-%{version} -n %{name}-%{version}
%if 0%{?el7}
%else
pathfix.py -pni "%{__python3} %{py3_shbang_opts}" . bin/ncgx bin/check_live bin/check_condor bin/check_ardiff
%endif

%build
%if 0%{?el7}
python setup.py build
%else
%py3_build
%endif

%install
%if 0%{?el7}
python setup.py install --single-version-externally-managed -O1 --root=$RPM_BUILD_ROOT --record=INSTALLED_FILES
%else
%{__python3} setup.py install --single-version-externally-managed -O1 --root=$RPM_BUILD_ROOT --record=INSTALLED_FILES
%endif

%clean
rm -rf $RPM_BUILD_ROOT

%post
if [ ! -f /usr/lib/ncgx/x_functions/__init__.py ]; then
    touch /usr/lib/ncgx/x_functions/__init__.py
fi

if [ ! -f /usr/lib/ncgx/x_plugins/__init__.py ]; then
    touch /usr/lib/ncgx/x_plugins/__init__.py
fi


%files -f INSTALLED_FILES
%defattr(-,root,root)
%doc README.md
