define host {
    use             ncgx-generic-host
    host_name       ::hostname
    alias           ::hostname
    address         ::ip
    check_command   ::host_check_command
    ::t_function(host_groups)
}
