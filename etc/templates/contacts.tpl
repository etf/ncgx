define contact {
	contact_name                    ::contact_name
	host_notifications_enabled		0
	service_notifications_enabled	1
	service_notification_period     ncgx-24x7
	host_notification_period        ncgx-24x7
    service_notification_options    w,u,r,c,f
	host_notification_options       d
	service_notification_commands   ncgx-notify-by-email
	host_notification_commands      ncgx-host-notify-by-email
	email			                ::contact_email
}
