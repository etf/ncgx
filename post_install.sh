if [ ! -f /usr/lib/ncgx/x_functions/__init__.py ]; then
    touch /usr/lib/ncgx/x_functions/__init__.py
fi

if [ ! -f /usr/lib/ncgx/x_plugins/__init__.py ]; then
    touch /usr/lib/ncgx/x_plugins/__init__.py
fi
