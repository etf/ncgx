import ncgx
import os

from setuptools import setup

NAME = 'ncgx'
VERSION = ncgx.VERSION
DESCRIPTION = "Nagios configuration generator"
LONG_DESCRIPTION = """
Nagios configuration generator
"""
AUTHOR = ncgx.AUTHOR
AUTHOR_EMAIL = ncgx.AUTHOR_EMAIL
LICENSE = "ASL 2.0"
PLATFORMS = "Any"
URL = "https://github.com/marian-babik/ncgx"
CLASSIFIERS = [
    "Development Status :: 5 - Production/Stable",
    "License :: OSI Approved :: Apache Software License",
    "Operating System :: Unix",
    "Programming Language :: Python",
    "Programming Language :: Python :: 2.6",
    "Programming Language :: Python :: 2.7",
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3.0",
    "Programming Language :: Python :: 3.1",
    "Programming Language :: Python :: 3.2",
    "Programming Language :: Python :: 3.3",
    "Topic :: Software Development :: Libraries :: Python Modules"
]


def get_files(install_prefix, directory):
    files = []
    for root, dirnames, filenames in os.walk(directory):
        subdir_files = []
        for filename in filenames:
            subdir_files.append(os.path.join(root, filename))
        if filenames and subdir_files:
            rel_path = os.path.relpath(root, directory)
            if rel_path != '.':
                files.append((os.path.join(install_prefix, rel_path), subdir_files))
            else:
                files.append((install_prefix, subdir_files))
    return files


TEMPLATES = get_files('/etc/ncgx/templates', 'etc/templates')
METRICS = get_files('/etc/ncgx/metrics.d', 'etc/metrics.d')

setup(name=NAME,
      version=VERSION,
      description=DESCRIPTION,
      long_description=LONG_DESCRIPTION,
      author=AUTHOR,
      author_email=AUTHOR_EMAIL,
      license=LICENSE,
      platforms=PLATFORMS,
      url=URL,
      classifiers=CLASSIFIERS,
      keywords='operations nagios configuration',
      packages=['ncgx', 'ncgx.config', 'ncgx.plugins', 'ncgx.scripts', 'ncgx.templates', 'ncgx.templates.functions'],
      install_requires=['argparse', 'requests'],
      data_files=[
          ('/usr/bin', ['bin/ncgx', 'bin/etf_resume_testing.sh',
                        'bin/etf_suspend_testing.sh', 'bin/etf-init.sh']),
          ('/var/cache/ncgx', ['cache/empty.txt']),
          ('/usr/lib/ncgx/x_functions', ['lib/x_functions/empty.txt']),
          ('/usr/lib/ncgx/x_plugins', ['lib/x_plugins/empty.txt']),
          ('/usr/lib64/nagios/plugins', ['bin/check_random', 'bin/check_dirsize', 'bin/check_ncgx',
                                         'bin/check_condor', 'bin/check_ardiff', 'bin/check_live']),
      ] + TEMPLATES + METRICS
      )
